<?php
require_once "../../vendor/autoload.php";
use App\Core\Input;
use App\Core\Session;
use App\Core\Unique;
use App\Core\Redirect;
use App\Core\Validation;
session_start();
//print_r($_POST);exit();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if (Input::exists('post')){
        if (Validation::valid(Input::get('email')) && Validation::valid(Input::get('regNo'))){
            if (Validation::emailValidation(Input::get('email'))){
                if (Unique::checkStudentEmail(Input::get('email'))){

                    $studentData = Unique::getStudentData();

                    if (Input::get('email') === $studentData->student_email && Input::get('regNo') === $studentData->registration_no){
                         Session::put('student', $studentData->student_id);
                        Redirect::to('view_result.php');
                    }else{
                        Session::put('error', 'Email or password not match. please try again from student login.');
                        Redirect::to('view_result.php');
                    }
                }else{
                    Session::put('error', 'Email or password not valid.');
                    Redirect::to('../../index.php');
                }
            }else{
                Session::put('error', 'Email not valid !');
                Redirect::to('../../index.php');
            }
        }else{
            Session::put('error', 'Invalid input !');
            Redirect::to('../../index.php');
        }
    }else{
        Session::put('error', 'Email or Password not be empty!');
        Redirect::to('../../index.php');
    }
}else{
    Redirect::to('../../index.php');
}
